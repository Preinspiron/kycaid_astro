import $ from "jquery";
import debounce from "../helpers/debounce";

const elements = {
  toggle: $("[data-nav-toogle]"),
  nav: $("[data-nav-menu]"),
  navList: $("[data-nav-list]"),
};

let eventListenersAdded = false;

const toggleShowMenu = () => {
  elements.nav.toggleClass("show-menu");
  elements.toggle.toggleClass("show-icon");

  if (elements.nav.hasClass("show-menu")) {
    $("body").css("overflow", "hidden");
  } else {
    $("body").css("overflow", "auto");
  }
};

const toggleShowSubMenu = (event) => {
  const target = $(event.target).closest("[data-dropdown-btn]");

  if (target.length) {
    const subMenu = target.next();
    const subMenuOpen = $(".show-sub-menu");

    if (!subMenuOpen.is(subMenu) && subMenuOpen.hasClass("show-sub-menu")) {
      subMenuOpen.removeClass("show-sub-menu").slideUp();
      subMenuOpen.prev().children(":last-child").removeClass("change-arrow");
    }

    subMenu.toggleClass("show-sub-menu").slideToggle();
    target.children(":last-child").toggleClass("change-arrow");
  }
};

const addEventListeners = () => {
  elements.toggle.on("click", toggleShowMenu);
  elements.navList.on("click", toggleShowSubMenu);
};

const removeEventListeners = () => {
  elements.toggle.off("click", toggleShowMenu);
  elements.navList.off("click", toggleShowSubMenu);
};

const screenWidthHandler = () => {
  const screenWidth = window.innerWidth;

  if (screenWidth <= 1023 && !eventListenersAdded) {
    addEventListeners();
    eventListenersAdded = true;
  } else if (screenWidth > 1023 && eventListenersAdded) {
    const showMenu = elements.nav.hasClass("show-menu");
    const showSubMenu = $(".show-sub-menu");
    if (showMenu) {
      toggleShowMenu();
    }
    if (showSubMenu.length) {
      showSubMenu.removeClass("show-sub-menu").slideUp();
      showSubMenu.prev().children(":last-child").removeClass("change-arrow");
    }
    removeEventListeners();
    eventListenersAdded = false;
  }
};

screenWidthHandler();

$(window).on("resize", debounce(screenWidthHandler, 500));
