import Splide from "@splidejs/splide";

const splide = new Splide(".splide", {
  arrows: false,

  fixedWidth: "92%",

  gap: 8,
  classes: {
    list: "splide__list verification__list",
    pagination: "verification__scroll flex",
    page: "item flex",
  },
  mediaQuery: "min",
  breakpoints: {
    1023: {
      destroy: true,
    },
  },
  mediaQuery: "max",
  breakpoints: {
    1022: {
      fixedWidth: 450,
    },
  },
});
const chekWorks = new Splide(".chek-works", {
  arrows: false,
  fixedWidth: "83%",
  pagination: false,
  gap: 8,

  classes: {
    root: "chek-works",
    track: "chek-works__track",
    list: "splide__track chek-works__list",
    slide: "chek-works__slide",
  },
  mediaQuery: "min",
  breakpoints: {
    767: {
      destroy: true,
    },
  },
});

splide.mount();
chekWorks.mount();
