function throttle(func, delay) {
  let isThrottled = false;
  return function () {
    const context = this;
    const args = arguments;
    if (!isThrottled) {
      func.apply(context, args);
      isThrottled = true;
      setTimeout(() => {
        isThrottled = false;
      }, delay);
    }
  };
}

export default throttle;
