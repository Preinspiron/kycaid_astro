import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

function animateDashboardMobile() {
  const tl = gsap.timeline({
    repeat: 0,
    delay: 0.3,
  });

  animateDashboard(tl);
}

function animateDashboardDesktop() {
  const tl = gsap.timeline({
    repeat: 0,
    scrollTrigger: {
      trigger: ".dashboard",
      start: "bottom bottom-=50",
      end: "bottom+=10 bottom",
      // markers: true,
    },
  });

  animateDashboard(tl);
}

function animateDashboard(tl) {
  tl.to(".dashboard", {
    duration: 1.5,
    rotateX: 0,
    perspective: 0,
    scale: 1,
    ease: "power2.inOut",
  });

  tl.fromTo(
    ".dashboard",
    {
      duration: 1,
      ease: "slow(0.7,0.7,false)",
      filter: "drop-shadow(0 0 0 rgba(0, 102, 255, 0))",
    },
    {
      filter: "drop-shadow(1px 1px 50px rgba(0, 102, 255, 0.5))",
    },
    "-=1.5",
  );

  tl.set(".dashboard", {
    filter: "none",
  });

  tl.to(
    [
      ".skeleton-menu-left",
      ".skeleton-header",
      ".skeleton-upgrade",
      ".skeleton-content",
      ".dashboard__menu-img",
      ".dashboard__main-img",
    ],
    {
      duration: 2,
      opacity: 1,
      animation: false,
      background: "none",
      ease: "slow(0.7,0.7,false)",
      stagger: 0.1,
    },
    "-=0.5",
  );
}

const mm = gsap.matchMedia();

mm.add("(max-width: 767px)", animateDashboardMobile).add(
  "(min-width: 768px)",
  animateDashboardDesktop,
);
