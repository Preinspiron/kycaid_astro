import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { ScrollToPlugin } from "gsap/ScrollToPlugin";
import { Observer } from "gsap/Observer";
import { Draggable } from "gsap/Draggable";

// import "@splidejs/splide/css/core";
// import $ from "jquery";
// window.jQuery = window.$ = $;

gsap.registerPlugin(ScrollTrigger, ScrollToPlugin);

const mm = gsap.matchMedia();
mm.add("(max-width: 767px)", () => {}).add("(min-width: 1024px)", () => {
  const verificationContainer = document.querySelector(
    ".verification__container",
  );
  const images = gsap.utils.toArray(".verification__list--dt .item");
  const items = gsap.utils
    .toArray(".verification__list .item")
    .forEach((item, i) => {
      gsap.to(images[i], {
        // opacity: 1,
        // ease: "none",
        duration: 0,
        // color: "red",
        scrollTrigger: {
          trigger: item,
          // scrub: true,
          // markers: true,
          start: "top center",
          end: "bottom+=64 center",
          toggleClass: {
            targets: [item, images[i]],
            className: "active",
          },
        },
      });
    });
  const dt = gsap.to(".verification__list--dt", {
    opacity: 1,
    duration: 2,
    // repeat: true,
  });

  ScrollTrigger.create({
    // trigger: ".verification__list--dt",
    // animation: dt,
    // toggleActions: "play reverse",
    start: "center center",
    end: () =>
      `+=${verificationContainer.offsetHeight - (72 * 2 + 450) + 155} center`,
    // scrub: true,
    pin: "#test2",
    // markers: true,
  });

  // ScrollTrigger.create({
  //   trigger: ".verification__container",
  //   animation: dt,
  //   // toggleActions: "play",
  //   start: "top center",
  //   end: "bottom center",
  //   // scrub: true,
  //   // pin: "#test2",
  //   markers: true,
  // });
});
