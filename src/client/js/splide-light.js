import Splide from "@splidejs/splide";

const splide = new Splide(".splide", {
  arrows: false,
  pagination: true,
  gap: "8px",
  mediaQuery: "min",
  breakpoints: {
    1024: {
      destroy: true,
    },
  },
});

splide.mount();
