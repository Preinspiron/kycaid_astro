const elements = {
  openModalContact: document.querySelector("[data-modal-contact-open]"),
  closeModalBtn: document.querySelector("[data-modal-close]"),
  submitBtn: document.querySelector("[data-modal-submit]"),
  modalContacts: document.querySelector("[data-modal-contacts]"),
  modalSuccess: document.querySelector("[data-modal-success]"),
  backDrop: document.querySelector("[data-backdrop]"),
};

const removeListener = () => {
  document.removeEventListener("keydown", closeModal);
  elements.backDrop.removeEventListener("click", closeModal);
  elements.closeModalBtn.removeEventListener("click", closeModal);
  elements.submitBtn.removeEventListener("click", openModalSuccess);
};

const closeModal = (event) => {
  if (
    event.code === "Escape" ||
    event.target === elements.backDrop ||
    event.currentTarget === elements.closeModalBtn
  ) {
    elements.modalContacts.classList.toggle("is-hidden");
    elements.backDrop.classList.toggle("is-hidden");
    removeListener();
  }
};

const closeModalSuccess = (event) => {
  if (event.code === "Escape" || event.target === elements.backDrop) {
    elements.modalSuccess.classList.toggle("is-hidden-success");
    elements.backDrop.classList.toggle("is-hidden");

    document.removeEventListener("keydown", closeModalSuccess);
    elements.backDrop.removeEventListener("click", closeModalSuccess);
  }
};

const openModal = () => {
  elements.modalContacts.classList.toggle("is-hidden");
  elements.backDrop.classList.toggle("is-hidden");

  document.addEventListener("keydown", closeModal);
  elements.backDrop.addEventListener("click", closeModal);
  elements.closeModalBtn.addEventListener("click", closeModal);
  elements.submitBtn.addEventListener("click", openModalSuccess);
};

const openModalSuccess = (event) => {
  event.preventDefault();
  elements.modalSuccess.classList.toggle("is-hidden-success");
  elements.modalContacts.classList.toggle("is-hidden");

  removeListener();

  document.addEventListener("keydown", closeModalSuccess);
  elements.backDrop.addEventListener("click", closeModalSuccess);
};

elements.openModalContact.addEventListener("click", openModal);
