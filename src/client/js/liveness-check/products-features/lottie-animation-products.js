import lottie from "lottie-web";

import animationData1 from "@assets/pages/liveness-check/lottie-animations/active.json";
import animationData2 from "@assets/pages/liveness-check/lottie-animations/passive.json";
import animationData3 from "@assets/pages/liveness-check/lottie-animations/Anti-fraud-mechanism.json";

const containers = document.querySelectorAll(".lottie-animation-product");

const addLottieAnimation = (container, data) => {
  const anim = lottie.loadAnimation({
    container: container,
    renderer: "svg",
    loop: true,
    autoplay: true,
    animationData: data,
  });
};

addLottieAnimation(containers[0], animationData1);
addLottieAnimation(containers[1], animationData2);
addLottieAnimation(containers[2], animationData3);
