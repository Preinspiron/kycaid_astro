import Splide from "@splidejs/splide";

document.addEventListener("DOMContentLoaded", function () {
  var main = new Splide("#main-carousel", {
    type: "fade",
    rewind: true,
    pagination: false,
    arrows: false,
  });

  var thumbnails = new Splide("#thumbnail-carousel", {
    autoWidth: true,
    gap: 8,
    rewind: true,
    pagination: false,
    isNavigation: true,
    arrows: false,
    mediaQuery: "min",
    breakpoints: {
      1024: {
        gap: 24,
      },
    },
  });

  main.sync(thumbnails);
  main.mount();
  thumbnails.mount();
});
