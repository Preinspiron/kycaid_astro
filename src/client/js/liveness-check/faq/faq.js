import $ from "jquery";

$(".faq__item").first().addClass("show-content");
$(".faq__item").first().find(".faq__content").slideDown();

const toggleContent = (event) => {
  const clickedItem = $(event.target).closest(".faq__item");
  if (clickedItem) {
    clickedItem.toggleClass("show-content");
    clickedItem.find(".faq__content").slideToggle();
  }
};

$("[data-faq-list]").on("click", toggleContent);
