import { ScrollTrigger } from "gsap/ScrollTrigger";
import { ScrollToPlugin } from "gsap/ScrollToPlugin";

gsap.registerPlugin(ScrollTrigger, ScrollToPlugin);
gsap.utils.toArray('[data-section="section"]').forEach((section) => {
  gsap.fromTo(
    section,
    {
      scale: 0.95,
    },
    {
      scale: 1,
      duration: 0.1,
      ease: "none",
      scrollTrigger: {
        trigger: section,
        start: "20% bottom-=20%",
        end: "bottom bottom-=20%",
      },
    },
  );
});
