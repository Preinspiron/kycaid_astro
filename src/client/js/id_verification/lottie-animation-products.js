import lottie from "lottie-web";

import animationData1 from "@assets/pages/id_verification/document_submisson.json";
import animationData2 from "@assets/pages/id_verification/data_extraction.json";
import animationData3 from "@assets/pages/id_verification/biometric_authentication.json";
import animationData4 from "@assets/pages/id_verification/database_checks.json";
import animationData5 from "@assets/pages/id_verification/realTimeVerification.json";

const containers = document.querySelectorAll(".lottie-animation-product");

const addLottieAmimation = (container, data) => {
  const anim = lottie.loadAnimation({
    container: container,
    renderer: "svg",
    loop: true,
    autoplay: true,
    animationData: data,
  });
};

addLottieAmimation(containers[0], animationData1);
addLottieAmimation(containers[1], animationData2);
addLottieAmimation(containers[2], animationData3);
addLottieAmimation(containers[3], animationData4);
addLottieAmimation(containers[4], animationData5);
