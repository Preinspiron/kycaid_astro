import lottie from "lottie-web";
import animationData from "../../assets/lottie-animations/onboarding.json";

const container = document.getElementById("lottie-animation");
const anim = lottie.loadAnimation({
  container: container,
  renderer: "svg",
  loop: true,
  autoplay: true,
  animationData: animationData,
});
