import "flag-icons/css/flag-icons.min.css";

import { feature } from "topojson-client";

import Globe from "globe.gl";
import countriesJson from "./datasets/ne_110m_admin_0_countries.topojson?raw";

async function fetchDataAndInitializeGlobe() {
  try {
    const countries = await JSON.parse(countriesJson);
    const world = Globe({
      animateIn: false,
      rendererConfig: {},
    })(document.getElementById("globeViz"));
    world
      // .width(300)

      // .height(300)

      .polygonCapColor("red")

      .lineHoverPrecision(0)
      .polygonsData(
        feature(countries, countries.objects.ne_110m_admin_0_countries)
          .features,
      )
      .polygonAltitude(0.4)
      .showGlobe(false)
      .showAtmosphere(false)
      .backgroundColor("rgba(0,0,0,0)")
      .polygonCapColor(() => "#BEC5CD")

      .polygonSideColor(() => "rgba(0,0,0,0)")
      .polygonStrokeColor(() => "#111")
      .polygonLabel(({ properties: d }) => {
        let flag = d.ISO_A2.toLowerCase();
        return `
        <div class="marker">123
        <svg width="231" height="175" viewBox="0 0 231 175" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M0.5 16C0.5 7.43959 7.43959 0.5 16 0.5H215C223.56 0.5 230.5 7.43959 230.5 16V152C230.5 160.56 223.56 167.5 215 167.5H121.266C120.3 167.5 119.42 168.057 119.006 168.93L116.856 173.47C116.314 174.614 114.686 174.614 114.144 173.47L111.994 168.93C111.58 168.057 110.7 167.5 109.734 167.5H16C7.43958 167.5 0.5 160.56 0.5 152V16Z" fill="white" stroke="#111111"/>
</svg>
        <span class="text fi fi-${flag}"></span>
          <b class='text' style="color:black">${d.ADMIN} </b> 
        </div>
        `;
      })
      .polygonsTransitionDuration(300);
    world.controls().autoRotate = true;
    world.controls().autoRotateSpeed = 0.35;
    const desctop = window.matchMedia("(min-width:1000px)");
    if (desctop.matches) return world.width(700).height(700);
    world.width(400).height(400);
    desctop.addEventListener("resieze", () => console.log(123));

    return world;
  } catch (error) {
    console.error("Error fetching or initializing globe:", error);
  }
}

async function initializeGlobe() {
  try {
    const world = await fetchDataAndInitializeGlobe();
    const features = world.polygonsData();

    function selectCountry(selectedCountry) {
      const selectedFeature = features.find(
        (feature) => feature.properties.ADMIN === selectedCountry,
      );

      if (selectedFeature) {
        const { bbox } = selectedFeature;
        const { coordinates } = selectedFeature.geometry;
        const lng = (bbox[0] + bbox[2]) / 2;
        const lat = (bbox[1] + bbox[3]) / 2;
        world.pointOfView({ lat, lng }, 1000);
        world.controls().autoRotate = false;
        world.polygonCapColor(({ properties }) =>
          properties.ADMIN === selectedCountry ? "#0066FF" : "#BEC5CD",
        );
      }
    }

    const countryList = features.map((country) => country.properties);
    const selector = document.createElement("select");
    selector.id = "country";

    countryList.forEach((country) => {
      const option = document.createElement("option");
      option.value = country.ADMIN;
      option.textContent = country.ADMIN;
      const flagIcon = document.createElement("span");
      if (country.ADMIN === "United States of America") {
        option.selected = true;
      }
      flagIcon.className = `fi fi-${country.ISO_A2.toLowerCase()}`;

      option.appendChild(flagIcon);
      selector.appendChild(option);
    });

    selector.addEventListener("change", (event) => {
      const selectedCountry = event.target.value;
      selectCountry(selectedCountry);
    });

    document.querySelector("[data-select-cuntry]").appendChild(selector);
  } catch (error) {
    console.error("Error initializing globe:", error);
  }
}

window.addEventListener("DOMContentLoaded", initializeGlobe);
