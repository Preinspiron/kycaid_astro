import Splide from "@splidejs/splide";

const splide = new Splide("#image-carousel", {
  arrows: false,
  pagination: true,

  gap: 24,

  // perPage: 4,
  focus: 0,

  mediaQuery: "max",
  breakpoints: {
    1028: {
      perPage: 3,
    },
    768: {
      perPage: 2,
    },
    431: {
      fixedWidth: "85%",
      perPage: 1,
    },
  },
});
splide.mount();
