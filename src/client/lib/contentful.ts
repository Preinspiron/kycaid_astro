// import contentful, { type EntryFieldTypes } from "contentful";
// import contentfulImport from "contentful-import";
// import { log } from "node_modules/astro/dist/core/logger/core";

// export interface BlogPost {
//   contentTypeId: "blogPost";
//   fields: {
//     title: EntryFieldTypes.Text;
//     content: EntryFieldTypes.RichText;
//     date: EntryFieldTypes.Date;
//     description: EntryFieldTypes.Text;
//     slug: EntryFieldTypes.Text;
//     img: EntryFieldTypes.Text;
//     img1: EntryFieldTypes.Object;
//   };
// }
// export interface Image {
//   contentTypeId: "image";
//   fields: {
//     test: EntryFieldTypes.Object;
//   };
// }

// export const contentfulClient = contentful.createClient({
//   space: import.meta.env.CONTENTFUL_SPACE_ID,
//   accessToken: import.meta.env.DEV
//     ? import.meta.env.CONTENTFUL_PREVIEW_TOKEN
//     : import.meta.env.CONTENTFUL_DELIVERY_TOKEN,
//   host: import.meta.env.DEV ? "preview.contentful.com" : "cdn.contentful.com",
// });

// const options = {
//   // content: {entries:..., contentTypes:..., locales:...},
//   content: { contentTypes: "image" },
//   spaceId: import.meta.env.CONTENTFUL_SPACE_ID,
//   managementToken: import.meta.env.CONTENTFUL_PREVIEW_TOKEN,
// };

// export const contenImport = contentfulImport(options)
//   .then(() => {
//     console.log("Data imported successfully");
//   })
//   .catch((err) => {
//     console.log("Oh no! Some errors occurred!", err);
//   });
