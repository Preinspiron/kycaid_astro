interface IConsts {
  images: {};
  screens: {
    mobile: string;
    tablet: string;
    laptop: object;
  };
  path: {
    title: string;
    description: string;
  };
}

export default {
  images: {},
  screens: {
    mobile: "376px",
    tablet: "768px",
    laptop: "1024px",
  },
  path: {
    "/": {
      title: "KYC Verification & Compliance Service - KYCAID.com",
      description:
        "Know Your Customer (KYC) Compliance Service ✓ Best-in-Class Fraud Detection ✓ Increase Conversion Rates ✓ Be verified with KYCAID.COM",
    },
    "/age-verification": {
      title: "Age Verification Service Online - KYCAID.com",
      description:
        "Age Verification Service ✓ Realtime Verification System ✓ Ready-to-Set Integration ✓ Be verified with KYCAID.COM",
    },
    "/liveness-check": {
      title: "Customer Liveness Check - KYCAID.com",
      description:
        "Liveness Check ✓ Realtime Verification System ✓ Ready-to-Set Integration ✓ Be verified with KYCAID.COM",
    },
    "/sanctions-pep-screening": {
      title: "Sanctions & PEP Screening - Your KYC Sanctions Check by KYCAID",
      description:
        "Sanctions, Watchlists and PEP Screening by KYCAID, Leading Provider of PEP Screening Services",
    },
    "/kyb": {
      title: "Know Your Business Compliance Service - KYB From KYCAID.com",
      description:
        "Know Your Business (KYB) Compliance Service ✓ Best-in-Class Fraud Detection ✓ Increase Conversion Rates ✓ Be verified with KYCAID.COM",
    },
  },
} as const;
