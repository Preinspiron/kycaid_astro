export const meta2 = {
  index: {
    title:
      "Trustworthy KYC Verification Service - Affordable KYC Checks From KYCAID",
    description:
      "Verify customer identity with ease using our KYC Verification Service. Trustworthy, efficient, and compliant. Simplify your verification process today.",
  },
  livenessCheck: {
    title: "Liveness Check & Facial Recognition KYC Verification",
    description:
      "Ensure user presence and prevent fraud with our Liveness Check. Cutting-edge technology for secure identity verification. Try it now.",
  },
};
