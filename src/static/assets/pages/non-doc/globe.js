import "flag-icons/css/flag-icons.min.css";
// import "./style.scss";
import Globe from "globe.gl";

async function fetchDataAndInitializeGlobe() {
  try {
    const res = await fetch(
      "/assets/pages/non-doc/datasets/ne_110m_admin_0_countries.geojson",
    );
    const countries = await res.json();

    const world = Globe()
      .width(700)
      .height(584)
      .backgroundColor("white")
      .globeImageUrl("//unpkg.com/three-globe/example/img/earth-night.jpg")
      .lineHoverPrecision(0)
      .polygonsData(countries.features)
      .polygonAltitude(0.05)
      .polygonCapColor("#111")
      .polygonSideColor(() => "#C0E8FF")
      .polygonStrokeColor(() => "#111")
      .polygonLabel(({ properties: d }) => {
        let flag = d.ISO_A2.toLowerCase();
        return `
          <span class="fi fi-${flag}"></span>
          <b>${d.ADMIN} (${d.ISO_A2}):</b> <br />
          GDP: <i>${d.GDP_MD_EST}</i> M$<br/>
          Population: <i>${d.POP_EST}</i>
        `;
      })
      .onPolygonHover((hoverD) =>
        world
          .polygonAltitude((d) => (d === hoverD ? 0.12 : 0.06))
          .polygonCapColor((d) => (d === hoverD ? "steelblue" : "#FEFEFF")),
      )
      .polygonsTransitionDuration(300)(document.getElementById("globeViz"));

    return world;
  } catch (error) {
    console.error("Error fetching or initializing globe:", error);
  }
}

async function initializeGlobe() {
  try {
    const world = await fetchDataAndInitializeGlobe();
    const features = world.polygonsData();

    function selectCountry(selectedCountry) {
      const selectedFeature = features.find(
        (feature) => feature.properties.ADMIN === selectedCountry,
      );

      if (selectedFeature) {
        const { coordinates } = selectedFeature.geometry;
        const lng = coordinates[0][0][0];
        const lat = coordinates[0][0][1];

        world.pointOfView({ lat, lng, altitude: 2.2 }, 1000);
      }
    }

    const countryList = features.map((country) => country.properties);
    const selector = document.createElement("select");

    countryList.forEach((country) => {
      const option = document.createElement("option");
      option.value = country.ADMIN;
      option.textContent = country.ADMIN;
      const flagIcon = document.createElement("span");
      console.log(country.ISO_A2.toLowerCase());
      flagIcon.className = `fi fi-${country.ISO_A2.toLowerCase()}`;

      option.appendChild(flagIcon);
      selector.appendChild(option);
    });

    selector.addEventListener("change", (event) => {
      const selectedCountry = event.target.value;
      selectCountry(selectedCountry);
    });

    document.querySelector("[data-select-cuntry]").appendChild(selector);
  } catch (error) {
    console.error("Error initializing globe:", error);
  }
}

window.addEventListener("DOMContentLoaded", initializeGlobe);
