import { defineConfig } from "astro/config";
import viteCompression from "vite-plugin-compression";

// https://astro.build/config
export default defineConfig({
  srcDir: "./src/client",

  build: {
    assets: "assets/img",
    format: "file",
  },
  // compressHTML: true,
  publicDir: "./src/static",
  outDir: "./public",
  cacheDir: "./_assets_cache",
  vite: {
    resolve: {
      alias: {
        "@datasets": "components/pages/non-doc/Globe/datasets",
      },
    },
    // plugins: [viteCompression()],
    css: {
      devSourcemap: true,
    },
    build: {
      rollupOptions: {
        output: {
          // chunkFileNames: "assets/js/chunk_[name]-[hash].js",
          entryFileNames: "assets/js/entry_[name]-[hash].js",
          assetFileNames: (data) => {
            if (/\.(gif|jpe?g|png|svg|webp)$/.test(data.name ?? "")) {
              return "assets/img/[name]-[hash][extname]";
            }
            // if (/\.(js)$/.test(data.name ?? "")) {
            //   return "assets/js/[name]-[hash][extname]";
            // }
            if (/\.css$/.test(data.name ?? "")) {
              return "assets/css/[name]-[hash][extname]";
            }
            if (/\.woff|woff2|ttf|otf|eot$/.test(data.name ?? "")) {
              return "assets/fonts/[name]-[hash][extname]";
            }
            return "assets/[name]-[hash][extname]";
          },
        },
      },
    },
  },
});
